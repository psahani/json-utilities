#include "utilities.h"
#include "file_utilities.h"
#include "log.h"

#include <malloc.h>
#include <string.h>
#include <stdlib.h>

typedef enum data_type_e {
	INVALID_DATA_TYPE = -1,
	DATA_TYPE_UINT8 = 0,
	DATA_TYPE_UINT16,
	DATA_TYPE_UINT32,
	DATA_TYPE_UINT64,
	DATA_TYPE_INT8,
	DATA_TYPE_INT16,
	DATA_TYPE_INT32,
	DATA_TYPE_INT64,
	DATA_TYPE_FLOAT32,
	DATA_TYPE_FLOAT64,
	DATA_TYPE_BOOL,
	DATA_TYPE_CHAR,
	DATA_TYPE_STRING,
	DATA_TYPE_UUID,
	DATA_TYPE_ENUM,
	DATA_TYPE_PTR
} DataType;

internal int32 writeComponentDefinition(
	const cJSON *component,
	FILE *file,
	Log log
);

internal void writeComponentData(const cJSON *component, FILE *file);
internal uint32 writeValueData(
	const char *dataTypeString,
	const cJSON *data,
	uint32 bytesWritten,
	FILE *file
);

internal DataType getDataType(const char *dataTypeString);
internal uint32 getDataTypeSize(DataType type);
internal uint32 getMaxStringSize(const char *dataTypeString);
internal uint32 getValueCount(const cJSON *data);
internal uint32 getComponentSize(const cJSON *component);

internal const uint8 binaryFileVersion = 1;
internal char *jsonFilename;

int32 exportEntity(const char *filename, Log log) {
	jsonFilename = getFullFilePath(filename, "json", NULL);
	cJSON *json = loadJSON(filename, log);

	if (!json) {
		LOG(log, "Invalid JSON file: %s\n", jsonFilename);
		free(jsonFilename);
		return -1;
	}

	char *entityFilename = getFullFilePath(filename, "entity", NULL);
	FILE *file = fopen(entityFilename, "wb");
	free(entityFilename);

	fwrite(&binaryFileVersion, sizeof(uint8), 1, file);

	cJSON *uuid = cJSON_GetObjectItem(json, "uuid");
	writeStringAsUUID(uuid->valuestring, file);

	cJSON *components = cJSON_GetObjectItem(json, "components");

	uint32 numComponents = getJSONListSize(components);
	fwrite(&numComponents, sizeof(uint32), 1, file);

	cJSON *component = components->child;
	while (component) {
		writeString(component->string, file);

		if (writeComponentDefinition(component, file, log) == -1) {
			return -1;
		}

		writeComponentData(component, file);

		component = component->next;
	}

	fclose(file);
	cJSON_Delete(json);
	free(jsonFilename);

	return 0;
}

int32 writeComponentDefinition(const cJSON *component, FILE *file, Log log) {
	uint32 numValues = getJSONListSize(component);
	fwrite(&numValues, sizeof(uint32), 1, file);

	if (numValues == 0) {
		LOG(log,
			"%s component is empty in %s\n",
			component->string,
			jsonFilename
		);

		return -1;
	}

	cJSON *value = component->child;
	while (value) {
		cJSON *name = cJSON_GetObjectItem(value, "name");
		writeString(name->valuestring, file);

		cJSON *data = name->next;

		int8 dataType = (int8)getDataType(data->string);
		fwrite(&dataType, sizeof(int8), 1, file);

		if ((DataType)dataType == INVALID_DATA_TYPE) {
			LOG(log,
				"Invalid data type '%s' in %s\n",
				data->string,
				jsonFilename
			);

			return -1;
		}

		if ((DataType)dataType == DATA_TYPE_STRING) {
			uint32 maxStringSize = getMaxStringSize(data->string);
			fwrite(&maxStringSize, sizeof(uint32), 1, file);
		}

		uint32 count = getValueCount(data);
		fwrite(&count, sizeof(uint32), 1, file);

		value = value->next;
	}

	return 0;
}

void writeComponentData(const cJSON *component, FILE *file) {
	uint32 componentSize = getComponentSize(component);
	fwrite(&componentSize, sizeof(uint32), 1, file);

	cJSON *value = component->child;
	uint32 bytesWritten = 0;

	while (value) {
		cJSON *data = cJSON_GetObjectItem(value, "name")->next;

		char *dataTypeString = data->string;
		if (getValueCount(data) == 1) {
			bytesWritten = writeValueData(
				dataTypeString,
				data,
				bytesWritten,
				file
			);
		} else {
			data = data->child;
			while (data) {
				bytesWritten = writeValueData(
					dataTypeString,
					data,
					bytesWritten,
					file
				);

				data = data->next;
			}
		}

		value = value->next;
	}

	uint32 padding = componentSize - bytesWritten;
	if (padding > 0) {
		void *paddingData = calloc(padding, 1);
		fwrite(paddingData, padding, 1, file);
		free(paddingData);
	}
}

uint32 writeValueData(
	const char *dataTypeString,
	const cJSON *data,
	uint32 bytesWritten,
	FILE *file
) {
	DataType dataType = getDataType(dataTypeString);

	uint32 size = 0;
	if (dataType == DATA_TYPE_STRING) {
		size = getMaxStringSize(dataTypeString);
	} else {
		size = getDataTypeSize(dataType);
	}

	uint32 paddingSize = size;
	if (dataType == DATA_TYPE_STRING || dataType == DATA_TYPE_UUID) {
		paddingSize = 1;
	}

	uint32 padding = bytesWritten % paddingSize;
	if (padding > 0) {
		padding = paddingSize - padding;
		void *paddingData = calloc(padding, 1);
		fwrite(paddingData, padding, 1, file);
		free(paddingData);
	}

	uint8 uint8Data = 0;
	uint16 uint16Data = 0;
	uint32 uint32Data = 0;
	uint64 uint64Data = 0;
	int8 int8Data = 0;
	int16 int16Data = 0;
	int32 int32Data = 0;
	int64 int64Data = 0;
	real32 real32Data = 0.0f;
	real64 real64Data = 0.0f;
	char *stringData = NULL;

	switch (dataType) {
		case DATA_TYPE_UINT8:
			uint8Data = (uint8)data->valueint;
			fwrite(&uint8Data, size, 1, file);
			break;
		case DATA_TYPE_UINT16:
			uint16Data = (uint16)data->valueint;
			fwrite(&uint16Data, size, 1, file);
			break;
		case DATA_TYPE_UINT32:
			uint32Data = (uint32)data->valueint;
			fwrite(&uint32Data, size, 1, file);
			break;
		case DATA_TYPE_UINT64:
		case DATA_TYPE_PTR:
			uint64Data = (uint64)data->valueint;
			fwrite(&uint64Data, size, 1, file);
			break;
		case DATA_TYPE_INT8:
			int8Data = (int8)data->valueint;
			fwrite(&int8Data, size, 1, file);
			break;
		case DATA_TYPE_INT16:
			int16Data = (int16)data->valueint;
			fwrite(&int16Data, size, 1, file);
			break;
		case DATA_TYPE_INT32:
		case DATA_TYPE_ENUM:
			int32Data = (int32)data->valueint;
			fwrite(&int32Data, size, 1, file);
			break;
		case DATA_TYPE_INT64:
			int64Data = (int64)data->valueint;
			fwrite(&int64Data, size, 1, file);
			break;
		case DATA_TYPE_FLOAT32:
			real32Data = (real32)data->valuedouble;
			fwrite(&real32Data, size, 1, file);
			break;
		case DATA_TYPE_FLOAT64:
			real64Data = (real64)data->valuedouble;
			fwrite(&real64Data, size, 1, file);
			break;
		case DATA_TYPE_BOOL:
			uint8Data = cJSON_IsTrue(data) ? true : false;
			fwrite(&uint8Data, size, 1, file);
			break;
		case DATA_TYPE_CHAR:
			fwrite(data->valuestring, size, 1, file);
			break;
		case DATA_TYPE_STRING:
			stringData = calloc(size, 1);
			strcpy(stringData, data->valuestring);
			fwrite(stringData, size, 1, file);
			free(stringData);
			break;
		case DATA_TYPE_UUID:
			writeStringAsUUID(data->valuestring, file);
			free(stringData);
			break;
		default:
			break;
	}

	return bytesWritten + size + padding;
}

DataType getDataType(const char *dataTypeString) {
	if (!strcmp(dataTypeString, "uint8")) {
		return DATA_TYPE_UINT8;
	} else if (!strcmp(dataTypeString, "uint16")) {
		return DATA_TYPE_UINT16;
	} else if (!strcmp(dataTypeString, "uint32")) {
		return DATA_TYPE_UINT32;
	} else if (!strcmp(dataTypeString, "uint64")) {
		return DATA_TYPE_UINT64;
	} else if (!strcmp(dataTypeString, "int8")) {
		return DATA_TYPE_INT8;
	} else if (!strcmp(dataTypeString, "int16")) {
		return DATA_TYPE_INT16;
	} else if (!strcmp(dataTypeString, "int32")) {
		return DATA_TYPE_INT32;
	} else if (!strcmp(dataTypeString, "int64")) {
		return DATA_TYPE_INT64;
	} else if (!strcmp(dataTypeString, "float32")) {
		return DATA_TYPE_FLOAT32;
	} else if (!strcmp(dataTypeString, "float64")) {
		return DATA_TYPE_FLOAT64;
	} else if (!strcmp(dataTypeString, "bool")) {
		return DATA_TYPE_BOOL;
	} else if (!strcmp(dataTypeString, "char")) {
		return DATA_TYPE_CHAR;
	} else if (strstr(dataTypeString, "char(")) {
		return DATA_TYPE_STRING;
	} else if (!strcmp(dataTypeString, "uuid")) {
		return DATA_TYPE_UUID;
	} else if (!strcmp(dataTypeString, "enum")) {
		return DATA_TYPE_ENUM;
	} else if (!strcmp(dataTypeString, "ptr")) {
		return DATA_TYPE_PTR;
	}

	return INVALID_DATA_TYPE;
}

uint32 getDataTypeSize(DataType type) {
	switch (type) {
		case DATA_TYPE_UINT8:
			return sizeof(uint8);
		case DATA_TYPE_INT8:
			return sizeof(int8);
		case DATA_TYPE_CHAR:
			return sizeof(char);
		case DATA_TYPE_BOOL:
			return sizeof(bool);
		case DATA_TYPE_UINT16:
			return sizeof(uint16);
		case DATA_TYPE_INT16:
			return sizeof(int16);
		case DATA_TYPE_UINT32:
			return sizeof(uint32);
		case DATA_TYPE_INT32:
			return sizeof(int32);
		case DATA_TYPE_ENUM:
			return sizeof(DataType);
		case DATA_TYPE_FLOAT32:
			return sizeof(real32);
		case DATA_TYPE_UINT64:
			return sizeof(uint64);
		case DATA_TYPE_PTR:
			return sizeof(void*);
		case DATA_TYPE_INT64:
			return sizeof(int64);
		case DATA_TYPE_FLOAT64:
			return sizeof(real64);
		case DATA_TYPE_UUID:
			return sizeof(UUID);
		default:
			break;
	}

	return 0;
}

uint32 getMaxStringSize(const char *dataTypeString) {
	char *a = strstr(dataTypeString, "(") + 1;
	char *b = strstr(dataTypeString, ")");
	char *sizeString = malloc(b - a + 1);

	memcpy(sizeString, a, b - a);
	sizeString[b - a] = '\0';

	uint32 size = atoi(sizeString);
	free(sizeString);

	return size;
}

uint32 getValueCount(const cJSON *data) {
	if (cJSON_IsArray(data)) {
		return getJSONListSize(data);
	}

	return 1;
}

uint32 getComponentSize(const cJSON *component) {
	uint32 size = 0;
	uint32 maxValueSize = 0;

	cJSON *value = component->child;
	while (value) {
		cJSON *data = cJSON_GetObjectItem(value, "name")->next;
		DataType dataType = getDataType(data->string);

		uint32 dataTypeSize = 0;
		if (dataType == DATA_TYPE_STRING) {
			dataTypeSize = getMaxStringSize(data->string);
		} else {
			dataTypeSize = getDataTypeSize(dataType);
		}

		uint32 valueCount = getValueCount(data);

		uint32 paddingSize = dataTypeSize;
		if (dataType == DATA_TYPE_STRING || dataType == DATA_TYPE_UUID) {
			paddingSize = 1;
		}

		if (paddingSize > maxValueSize) {
			maxValueSize = paddingSize;
		}

		uint32 padding = size % paddingSize;
		size += (padding > 0 ? paddingSize - padding : 0) +
				valueCount * dataTypeSize;

		value = value->next;
	}

	uint32 padding = size % maxValueSize;
	if (padding > 0) {
		size += maxValueSize - padding;
	}

	return size;
}