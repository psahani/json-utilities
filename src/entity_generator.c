#include "utilities.h"
#include "file_utilities.h"

#include <cjson/cJSON.h>

#include <stdlib.h>

int32 generateEntity(const char *filename, Log log) {
	cJSON *json = cJSON_CreateObject();

	cJSON_AddStringToObject(json, "uuid", generateUUID().string);
	cJSON_AddObjectToObject(json, "components");

	writeJSON(json, filename, true, log);
	cJSON_Delete(json);

	return 0;
}