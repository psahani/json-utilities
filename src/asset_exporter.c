#include "utilities.h"
#include "file_utilities.h"
#include "entity_utilities.h"
#include "log.h"

#include <malloc.h>
#include <string.h>

#define COLLISION_PRIMITIVES_FOLDER_NAME "collision_primitives"

typedef enum collision_primitive_type_e {
	COLLISION_PRIMITIVE_TYPE_BOX,
	COLLISION_PRIMITIVE_TYPE_SPHERE,
	COLLISION_PRIMITIVE_TYPE_CAPSULE
} CollisionPrimitiveType;

typedef struct collision_primitive_entity_t {
	UUID uuid;
	UUID parent;
} CollisionPrimitiveEntity;

internal void writeMaterial(cJSON *material, FILE *file);
internal void writeMasks(cJSON *masks, FILE *file);
internal void writeEntity(
	UUID entityID,
	cJSON *centerOfMass,
	const char *filename,
	UUID collisionTreeID,
	bool hasCollisionPrimitiveSiblings,
	bool hasCollisionPrimitives,
	UUID firstCollisionPrimitive,
	const char *skeleton,
	const char *idleAnimation,
	Log log
);

internal void writeCollisionPrimitives(
	UUID entityID,
	const char *skeleton,
	cJSON *boxes,
	cJSON *spheres,
	cJSON *capsules,
	const char *parentFolder,
	uint32 numCollisionPrimitives,
	CollisionPrimitiveEntity *collisionPrimitiveEntities,
	uint32 numCollisionPrimitiveSiblings,
	UUID *collisionPrimitiveSiblings,
	Log log
);

internal void writeCollisionPrimitive(
	UUID entityID,
	cJSON *collisionPrimitive,
	CollisionPrimitiveType collisionPrimitiveType,
	UUID uuid,
	UUID nextUUID,
	UUID parent,
	UUID nextSiblingUUID,
	const char *filename,
	Log log
);

internal void writeFloatArray(cJSON *array, FILE *file);

internal UUID getCollisionPrimitiveUUID(cJSON *collisionPrimitive);
internal UUID getCollisionPrimitiveParent(cJSON *collisionPrimitive);
internal UUID getNextCollisionPrimitiveEntity(
	uint32 *index,
	UUID *parent,
	uint32 numCollisionPrimitives,
	CollisionPrimitiveEntity *collisionPrimitiveEntities
);

internal UUID getNextCollisionPrimitiveSibling(
	uint32 *index,
	uint32 numCollisionPrimitiveSiblings,
	UUID *collisionPrimitiveSiblings
);

internal const uint8 binaryFileVersion = 1;

int32 exportAsset(const char *filename, Log log) {
	char *jsonFilename = getFullFilePath(filename, "json", NULL);
	cJSON *json = loadJSON(filename, log);

	if (!json) {
		LOG(log, "Invalid JSON file: %s\n", jsonFilename);
		free(jsonFilename);
		return -1;
	}

	free(jsonFilename);

	char *assetFilename = getFullFilePath(filename, "asset", NULL);
	FILE *file = fopen(assetFilename, "wb");
	free(assetFilename);

	fwrite(&binaryFileVersion, sizeof(uint8), 1, file);

	cJSON *subsets = cJSON_GetObjectItem(json, "meshes");

	uint32 numSubsets = getJSONListSize(subsets);
	fwrite(&numSubsets, sizeof(uint32), 1, file);

	cJSON *subset = subsets->child;
	while (subset) {
		writeString(subset->string, file);
		subset = subset->next;
	}

	subset = subsets->child;
	while (subset) {
		cJSON *material = cJSON_GetObjectItem(subset, "material");
		writeMaterial(material, file);

		cJSON *masks = cJSON_GetObjectItem(subset, "masks");
		writeMasks(masks, file);

		subset = subset->next;
	}

	cJSON *physics = cJSON_GetObjectItem(json, "physics");
	cJSON *collisionPrimitives = cJSON_GetObjectItem(
		physics,
		"collision_primitives"
	);

	cJSON *boxes = cJSON_GetObjectItem(collisionPrimitives, "boxes");
	cJSON *spheres = cJSON_GetObjectItem(collisionPrimitives, "spheres");
	cJSON *capsules = cJSON_GetObjectItem(collisionPrimitives, "capsules");

	UUID entityID = generateUUID();

	uint32 numCollisionPrimitives =
		getJSONListSize(boxes) +
		getJSONListSize(spheres) +
		getJSONListSize(capsules);

	CollisionPrimitiveEntity *collisionPrimitiveEntities = calloc(
		numCollisionPrimitives,
		sizeof(CollisionPrimitiveEntity)
	);

	uint32 i = 0;

	cJSON *box = boxes->child;
	while (box) {
		collisionPrimitiveEntities[i].uuid = getCollisionPrimitiveUUID(box);
		collisionPrimitiveEntities[i++].parent =
			getCollisionPrimitiveParent(box);
		box = box->next;
	}

	cJSON *sphere = spheres->child;
	while (sphere) {
		collisionPrimitiveEntities[i].uuid = getCollisionPrimitiveUUID(sphere);
		collisionPrimitiveEntities[i++].parent =
			getCollisionPrimitiveParent(sphere);
		sphere = sphere->next;
	}

	cJSON *capsule = capsules->child;
	while (capsule) {
		collisionPrimitiveEntities[i].uuid = getCollisionPrimitiveUUID(capsule);
		collisionPrimitiveEntities[i++].parent =
			getCollisionPrimitiveParent(capsule);
		capsule = capsule->next;
	}

	uint32 numCollisionPrimitiveSiblings = 0;
	UUID *collisionPrimitiveSiblings = calloc(
		numCollisionPrimitives,
		sizeof(UUID)
	);

	for (uint32 i = 0; i < numCollisionPrimitives; i++) {
		CollisionPrimitiveEntity *entity = &collisionPrimitiveEntities[i];
		if (strlen(entity->parent.string) == 0) {
			strcpy(
				collisionPrimitiveSiblings[
					numCollisionPrimitiveSiblings++
				].string,
				entity->uuid.string
			);
		}
	}

	collisionPrimitiveSiblings = realloc(
		collisionPrimitiveSiblings,
		numCollisionPrimitiveSiblings * sizeof(UUID)
	);

	UUID collisionTreeID = {};
	if (numCollisionPrimitives > 0) {
		collisionTreeID = collisionPrimitiveEntities[0].uuid;
	}

	UUID firstCollisionPrimitive = {};
	if (numCollisionPrimitiveSiblings > 0) {
		firstCollisionPrimitive = collisionPrimitiveSiblings[0];
	}

	cJSON *centerOfMass = cJSON_GetObjectItem(physics, "center_of_mass");
	cJSON *animation = cJSON_GetObjectItem(json, "animation");

	char *skeleton = NULL;
	const char *idleAnimation = NULL;

	if (animation) {
		const char *skeletonString = cJSON_GetObjectItem(
			animation,
			"skeleton"
		)->valuestring;
		skeleton = malloc(strlen(skeletonString) + 1);
		strcpy(skeleton, skeletonString);

		entityID = stringToUUID(
			cJSON_GetObjectItem(animation, "entity")->valuestring
		);

		idleAnimation = cJSON_GetObjectItem(
			animation,
			"names"
		)->child->string;
	}

	writeEntity(
		entityID,
		centerOfMass,
		filename,
		collisionTreeID,
		numCollisionPrimitiveSiblings > 0,
		numCollisionPrimitives > 0,
		firstCollisionPrimitive,
		skeleton,
		idleAnimation,
		log
	);

	cJSON_DeleteItemFromObject(json, "animations");

	if (animation) {
		cJSON *animationNames = cJSON_GetObjectItem(animation, "names");
		cJSON *animations = cJSON_Duplicate(animationNames, true);
		cJSON_DeleteItemFromObject(json, "animation");

		subsets = cJSON_Duplicate(subsets, true);
		physics = cJSON_Duplicate(physics, true);

		cJSON_DeleteItemFromObject(json, "meshes");
		cJSON_DeleteItemFromObject(json, "physics");

		cJSON_AddItemToObject(json, "animations", animations);
		cJSON_AddItemToObject(json, "meshes", subsets);
		cJSON_AddItemToObject(json, "physics", physics);

		collisionPrimitives = cJSON_GetObjectItem(
			physics,
			"collision_primitives"
		);

		boxes = cJSON_GetObjectItem(collisionPrimitives, "boxes");
		spheres = cJSON_GetObjectItem(collisionPrimitives, "spheres");
		capsules = cJSON_GetObjectItem(collisionPrimitives, "capsules");
	}

	char *parentFolder = getParentFolder(filename);
	writeCollisionPrimitives(
		entityID,
		skeleton,
		boxes,
		spheres,
		capsules,
		parentFolder,
		numCollisionPrimitives,
		collisionPrimitiveEntities,
		numCollisionPrimitiveSiblings,
		collisionPrimitiveSiblings,
		log
	);

	free(skeleton);
	free(parentFolder);

	free(collisionPrimitiveEntities);
	free(collisionPrimitiveSiblings);

	fclose(file);

	writeJSON(json, filename, true, log);
	cJSON_Delete(json);

	return 0;
}

void writeMaterial(cJSON *material, FILE *file) {
	cJSON *materialEntry = cJSON_GetObjectItem(material, "name");
	writeString(materialEntry->valuestring, file);

	if (strlen(materialEntry->valuestring) > 0) {
		materialEntry = cJSON_GetObjectItem(material, "double-sided");
		bool doubleSided = cJSON_IsTrue(materialEntry) ? true : false;
		fwrite(&doubleSided, sizeof(bool), 1, file);

		cJSON *materialValue = cJSON_GetObjectItem(material, "values")->child;
		while (materialValue) {
			writeFloatArray(materialValue, file);
			materialValue = materialValue->next;
		}
	}
}

void writeMasks(cJSON *masks, FILE *file) {
	cJSON *materialMask = cJSON_GetObjectItem(masks, "material");

	cJSON *materialMaskEntry = materialMask->child;
	while (materialMaskEntry) {
		writeMaterial(materialMaskEntry, file);
		materialMaskEntry = materialMaskEntry->next;
	}

	cJSON *opacityMask = cJSON_GetObjectItem(masks, "opacity");

	real32 opacityMaskValue = opacityMask->child->valuedouble;
	fwrite(&opacityMaskValue, sizeof(real32), 1, file);
}

void writeEntity(
	UUID entityID,
	cJSON *centerOfMass,
	const char *filename,
	UUID collisionTreeID,
	bool hasCollisionPrimitiveSiblings,
	bool hasCollisionPrimitives,
	UUID firstCollisionPrimitive,
	const char *skeleton,
	const char *idleAnimation,
	Log log
) {
	cJSON *json = cJSON_CreateObject();

	cJSON_AddStringToObject(json, "uuid", entityID.string);

	cJSON *components = cJSON_AddObjectToObject(json, "components");

	UUID skeletonID = stringToUUID(skeleton);

	addTransform(
		components,
		NULL,
		NULL,
		NULL,
		NULL,
		hasCollisionPrimitiveSiblings ? &firstCollisionPrimitive :
			skeleton ? &skeletonID : NULL,
		NULL
	);

	cJSON *model = cJSON_AddArrayToObject(components, "model");

	char *modelName = getFilename(filename);
	addString(model, "name", 64, modelName);
	free(modelName);

	addBool(model, "visible", true);

	if (hasCollisionPrimitives) {
		cJSON *rigidBody = cJSON_AddArrayToObject(components, "rigid_body");

		addInteger(rigidBody, "body ID", "ptr", 0);
		addInteger(rigidBody, "space ID", "ptr", 0);
		addBool(rigidBody, "enabled", true);
		addBool(rigidBody, "dynamic", false);
		addBool(rigidBody, "gravity", false);
		addRealNumber(rigidBody, "mass", "float32", 1.0);

		// TODO: Use proper moment of inertia matrix to match center of mass
		// real32 *centerOfMassValues = calloc(3, sizeof(real32));
		// getFloatArray(centerOfMass, 3, &centerOfMassValues);
		real32 values[] = { 0.0f, 0.0f, 0.0f };

		addFloatArray(rigidBody, "center of mass", 3, values);
		// free(centerOfMassValues);

		// real32 values[] = { 0.0f, 0.0f, 0.0f };
		addFloatArray(rigidBody, "velocity", 3, values);
		addFloatArray(rigidBody, "angular velocity", 3, values);

		addBool(rigidBody, "default damping", true);
		addRealNumber(rigidBody, "linear damping", "float32", 0.1);
		addRealNumber(rigidBody, "angular damping", "float32", 0.1);
		addRealNumber(rigidBody, "linear damping threshold", "float32", 0.1);
		addRealNumber(rigidBody, "angular damping threshold", "float32", 0.1);
		addRealNumber(rigidBody, "max angular speed", "float32", 0.0);
		addInteger(rigidBody, "moment of inertia type", "enum", 2);

		real32 moiValues[] = { 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f };
		addFloatArray(rigidBody, "moment of inertia", 6, moiValues);

		cJSON *collision = cJSON_AddArrayToObject(components, "collision");

		UUID emptyUUID = {};

		addUUID(collision, "collision tree", collisionTreeID);
		addUUID(collision, "hit list", emptyUUID);
		addUUID(collision, "last hit list", emptyUUID);
	}

	if (skeleton) {
		cJSON *animation = cJSON_AddArrayToObject(components, "animation");
		addUUID(animation, "skeleton", stringToUUID(skeleton));
		addString(animation, "idle animation", 64, idleAnimation);
		addRealNumber(animation, "speed", "float32", 1.0);
		addRealNumber(animation, "transition duration", "float64", 0.0);

		cJSON *animator = cJSON_AddArrayToObject(components, "animator");
		addString(animator, "current animation", 64, "");
		addRealNumber(animator, "time", "float64", 0.0);
		addRealNumber(animator, "duration", "float64", 0.0);
		addInteger(animator, "loop count", "int32", 0);
		addRealNumber(animator, "speed", "float32", 1.0);
		addBool(animator, "paused", false);
		addString(animator, "previous animation", 64, "");
		addRealNumber(animator, "previous animation time", "float64", 0.0);
		addRealNumber(animator, "transition time", "float64", 0.0);
		addRealNumber(animator, "transition duration", "float64", 0.0);
	}

	char *jsonFilename = concatenateStrings(filename, "_", "entity");
	writeJSON(json, jsonFilename, true, log);
	free(jsonFilename);

	cJSON_Delete(json);
}

void writeCollisionPrimitives(
	UUID entityID,
	const char *skeleton,
	cJSON *boxes,
	cJSON *spheres,
	cJSON *capsules,
	const char *parentFolder,
	uint32 numCollisionPrimitives,
	CollisionPrimitiveEntity *collisionPrimitiveEntities,
	uint32 numCollisionPrimitiveSiblings,
	UUID *collisionPrimitiveSiblings,
	Log log
) {
	char *collisionPrimitivesFolder = getFullFilePath(
		COLLISION_PRIMITIVES_FOLDER_NAME,
		NULL,
		parentFolder ? parentFolder : NULL
	);

	deleteFolder(collisionPrimitivesFolder, false, log);

	cJSON *box = boxes->child;
	cJSON *sphere = spheres->child;
	cJSON *capsule = capsules->child;

	if (!box && !sphere && !capsule) {
		free(collisionPrimitivesFolder);
		return;
	}

	MKDIR(collisionPrimitivesFolder);

	uint32 index = 0;

	UUID parent;
	UUID uuid = getNextCollisionPrimitiveEntity(
		&index,
		&parent,
		numCollisionPrimitives,
		collisionPrimitiveEntities
	);

	UUID nextParent;
	UUID nextUUID = getNextCollisionPrimitiveEntity(
		&index,
		&nextParent,
		numCollisionPrimitives,
		collisionPrimitiveEntities
	);

	uint32 siblingIndex = 1;
	UUID nextSiblingID = {};

	char filename[256];
	memset(filename, 0, 256);

	char lastFilename[256];
	memset(lastFilename, 0, 256);

	if (box) {
		char *boxesFolder = getFullFilePath(
			"boxes",
			NULL,
			collisionPrimitivesFolder
		);

		MKDIR(boxesFolder);

		for (uint32 i = 0; box; i++) {
			memset(filename, 0, 256);
			sprintf(filename, "%s/box_%d", boxesFolder, i);

			if (strlen(parent.string) > 0) {
				nextSiblingID = stringToUUID("");
			} else {
				nextSiblingID = getNextCollisionPrimitiveSibling(
					&siblingIndex,
					numCollisionPrimitiveSiblings,
					collisionPrimitiveSiblings
				);

				strcpy(lastFilename, filename);
			}

			writeCollisionPrimitive(
				entityID,
				box,
				COLLISION_PRIMITIVE_TYPE_BOX,
				uuid,
				nextUUID,
				parent,
				nextSiblingID,
				filename,
				log
			);

			uuid = nextUUID, parent = nextParent;
			nextUUID = getNextCollisionPrimitiveEntity(
				&index,
				&nextParent,
				numCollisionPrimitives,
				collisionPrimitiveEntities
			);

			box = box->next;
		}

		free(boxesFolder);
	}

	if (sphere) {
		char *spheresFolder = getFullFilePath(
			"spheres",
			NULL,
			collisionPrimitivesFolder
		);

		MKDIR(spheresFolder);

		for (uint32 i = 0; sphere; i++) {
			memset(filename, 0, 256);
			sprintf(filename, "%s/sphere_%d", spheresFolder, i);

			if (strlen(parent.string) > 0) {
				nextSiblingID = stringToUUID("");
			} else {
				nextSiblingID = getNextCollisionPrimitiveSibling(
					&siblingIndex,
					numCollisionPrimitiveSiblings,
					collisionPrimitiveSiblings
				);

				strcpy(lastFilename, filename);
			}

			writeCollisionPrimitive(
				entityID,
				sphere,
				COLLISION_PRIMITIVE_TYPE_SPHERE,
				uuid,
				nextUUID,
				parent,
				nextSiblingID,
				filename,
				log
			);

			uuid = nextUUID, parent = nextParent;
			nextUUID = getNextCollisionPrimitiveEntity(
				&index,
				&nextParent,
				numCollisionPrimitives,
				collisionPrimitiveEntities
			);

			sphere = sphere->next;
		}

		free(spheresFolder);
	}

	if (capsule) {
		char *capsulesFolder = getFullFilePath(
			"capsules",
			NULL,
			collisionPrimitivesFolder
		);

		MKDIR(capsulesFolder);

		for (uint32 i = 0; capsule; i++) {
			memset(filename, 0, 256);
			sprintf(filename, "%s/capsule_%d", capsulesFolder, i);

			if (strlen(parent.string) > 0) {
				nextSiblingID = stringToUUID("");
			} else {
				nextSiblingID = getNextCollisionPrimitiveSibling(
					&siblingIndex,
					numCollisionPrimitiveSiblings,
					collisionPrimitiveSiblings
				);

				strcpy(lastFilename, filename);
			}

			writeCollisionPrimitive(
				entityID,
				capsule,
				COLLISION_PRIMITIVE_TYPE_CAPSULE,
				uuid,
				nextUUID,
				parent,
				nextSiblingID,
				filename,
				log
			);

			uuid = nextUUID, parent = nextParent;
			nextUUID = getNextCollisionPrimitiveEntity(
				&index,
				&nextParent,
				numCollisionPrimitives,
				collisionPrimitiveEntities
			);

			capsule = capsule->next;
		}

		free(capsulesFolder);
	}

	free(collisionPrimitivesFolder);

	if (strlen(lastFilename) > 0) {
		cJSON *json = loadJSON(lastFilename, log);

		cJSON *components = cJSON_GetObjectItem(json, "components");

		cJSON *transform = cJSON_GetObjectItem(components, "transform");

		cJSON *nextSibling = transform->child->next->next->next->next->next;
		cJSON_DeleteItemFromObject(nextSibling, "uuid");
		cJSON_AddStringToObject(nextSibling, "uuid", skeleton ? skeleton : "");

		writeJSON(json, lastFilename, true, log);
		cJSON_Delete(json);
	}
}

void writeCollisionPrimitive(
	UUID entityID,
	cJSON *collisionPrimitive,
	CollisionPrimitiveType collisionPrimitiveType,
	UUID uuid,
	UUID nextUUID,
	UUID parent,
	UUID nextSiblingUUID,
	const char *filename,
	Log log
) {
	cJSON *json = cJSON_CreateObject();

	cJSON_AddStringToObject(json, "uuid", uuid.string);

	cJSON *components = cJSON_AddObjectToObject(json, "components");

	real32 *positionValues = calloc(3, sizeof(real32));

	getFloatArray(
		cJSON_GetObjectItem(collisionPrimitive, "position"),
		3,
		positionValues
	);

	real32 *rotationValues = calloc(4, sizeof(real32));
	rotationValues[3] = 1.0f;

	getFloatArray(
		cJSON_GetObjectItem(collisionPrimitive, "rotation"),
		4,
		rotationValues
	);

	real32 *scaleValues = calloc(3, sizeof(real32));
	for (uint8 i = 0; i < 3; i++) {
		scaleValues[i] = 1.0f;
	}

	getFloatArray(
		cJSON_GetObjectItem(collisionPrimitive, "scale"),
		3,
		scaleValues
	);

	for (uint8 i = 0; i < 3; i++) {
		if (scaleValues[i] < 0.0f) {
			scaleValues[i] = 0.0f;
		}
	}

	addTransform(
		components,
		positionValues,
		collisionPrimitiveType == COLLISION_PRIMITIVE_TYPE_SPHERE ?
			NULL : rotationValues,
		NULL,
		strlen(parent.string) > 0 ? &parent : &entityID,
		NULL,
		&nextSiblingUUID
	);

	cJSON *box, *sphere, *capsule;

	real32 bounds[] = {
		scaleValues[0] / 2.0f,
		scaleValues[1] / 2.0f,
		scaleValues[2] / 2.0f
	};

	real32 radius = MAX(bounds[0], bounds[1]);
	uint8 largestScale = 0;
	real32 length = 0.0f;

	switch (collisionPrimitiveType) {
		case COLLISION_PRIMITIVE_TYPE_BOX:
			box = cJSON_AddArrayToObject(components, "box");
			addFloatArray(box, "bounds", 3, bounds);
			break;
		case COLLISION_PRIMITIVE_TYPE_SPHERE:
			sphere = cJSON_AddArrayToObject(components, "sphere");
			addRealNumber(
				sphere,
				"radius",
				"float32",
				MAX(MAX(bounds[0], bounds[1]), bounds[2])
			);

			break;
		case COLLISION_PRIMITIVE_TYPE_CAPSULE:
			capsule = cJSON_AddArrayToObject(components, "capsule");

			if (scaleValues[0] != scaleValues[1] &&
				scaleValues[0] != scaleValues[2] &&
				scaleValues[1] != scaleValues[2]) {
				if (scaleValues[1] > scaleValues[largestScale]) {
					largestScale = 1;
				}

				if (scaleValues[2] > scaleValues[largestScale]) {
					largestScale = 2;
				}

				if (largestScale == 0) {
					radius = MAX(bounds[1], bounds[2]);
				} else if (largestScale == 1) {
					radius = MAX(bounds[0], bounds[2]);
				}

				length = scaleValues[largestScale];
			} else {
				if (scaleValues[0] == scaleValues[1]) {
					radius = bounds[0];
					length = scaleValues[2];
				} else if (scaleValues[0] == scaleValues[2]) {
					radius = bounds[0];
					length = scaleValues[1];
				} else {
					radius = bounds[1];
					length = scaleValues[0];
				}
			}

			length -= 2 * radius;

			if (length < 0.0f) {
				length = 0.0f;
			}

			addRealNumber(capsule, "radius", "float32", radius);
			addRealNumber(capsule, "length", "float32", length);

			break;
		default:
			break;
	}

	free(positionValues);
	free(rotationValues);
	free(scaleValues);

	cJSON *collisionTreeNode = cJSON_AddArrayToObject(
		components,
		"collision_tree_node"
	);

	addInteger(collisionTreeNode, "type", "enum", collisionPrimitiveType);
	addUUID(collisionTreeNode, "collision volume", entityID);
	addUUID(collisionTreeNode, "next collider", nextUUID);
	addBool(collisionTreeNode, "is trigger", false);
	addInteger(collisionTreeNode, "geometry ID", "ptr", 0);

	writeJSON(json, filename, true, log);
	cJSON_Delete(json);
}

void writeFloatArray(cJSON *array, FILE *file) {
	array = array->child;
	while (array) {
		real32 value = (real32)array->valuedouble;
		fwrite(&value, sizeof(real32), 1, file);
		array = array->next;
	}
}

UUID getCollisionPrimitiveUUID(cJSON *collisionPrimitive) {
	cJSON *entity = cJSON_GetObjectItem(collisionPrimitive, "entity");
	UUID uuid = {};
	if (entity) {
		uuid = stringToUUID(entity->valuestring);
	} else {
		uuid = generateUUID();
	}

	cJSON_DeleteItemFromObject(collisionPrimitive, "entity");
	return uuid;
}

UUID getCollisionPrimitiveParent(cJSON *collisionPrimitive) {
	UUID uuid = stringToUUID("");

	cJSON *parent = cJSON_GetObjectItem(collisionPrimitive, "parent");
	if (parent) {
		uuid = stringToUUID(parent->valuestring);
		cJSON_DeleteItemFromObject(collisionPrimitive, "parent");
	}

	return uuid;
}

UUID getNextCollisionPrimitiveEntity(
	uint32 *index,
	UUID *parent,
	uint32 numCollisionPrimitives,
	CollisionPrimitiveEntity *collisionPrimitiveEntities
) {
	UUID uuid = stringToUUID("");

	if (*index < numCollisionPrimitives) {
		uuid = collisionPrimitiveEntities[*index].uuid;
		*parent = collisionPrimitiveEntities[*index].parent;
		(*index)++;
	}

	return uuid;
}

UUID getNextCollisionPrimitiveSibling(
	uint32 *index,
	uint32 numCollisionPrimitiveSiblings,
	UUID *collisionPrimitiveSiblings
) {
	UUID uuid = stringToUUID("");

	if (*index < numCollisionPrimitiveSiblings) {
		uuid = collisionPrimitiveSiblings[*index];
		(*index)++;
	}

	return uuid;
}