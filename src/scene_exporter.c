#include "utilities.h"
#include "file_utilities.h"
#include "log.h"

#include <malloc.h>

internal const uint8 binaryFileVersion = 1;

internal void writeSystemGroup(const cJSON *systemGroup, FILE *file);

int32 exportScene(const char *filename, Log log) {
	char *jsonFilename = getFullFilePath(filename, "json", NULL);
	cJSON *json = loadJSON(filename, log);

	if (!json) {
		LOG(log, "Invalid JSON file: %s\n", jsonFilename);
		free(jsonFilename);
		return -1;
	}

	free(jsonFilename);

	char *sceneFilename = getFullFilePath(filename, "scene", NULL);
	FILE *file = fopen(sceneFilename, "wb");
	free(sceneFilename);

	fwrite(&binaryFileVersion, sizeof(uint8), 1, file);

	cJSON *systemGroups = cJSON_GetObjectItem(json, "systems");

	uint32 numSystemGroups = getJSONListSize(systemGroups);
	fwrite(&numSystemGroups, sizeof(uint32), 1, file);

	cJSON *systemGroup = systemGroups->child;
	while (systemGroup) {
		writeSystemGroup(systemGroup, file);
		systemGroup = systemGroup->next;
	}

	cJSON *componentLimits = cJSON_GetObjectItem(json, "component_limits");

	uint32 numComponentLimits = getJSONListSize(componentLimits);
	fwrite(&numComponentLimits, sizeof(uint32), 1, file);

	cJSON *componentLimit = componentLimits->child;
	while (componentLimit) {
		writeString(componentLimit->string, file);

		uint32 componentLimitNumber = (uint32)componentLimit->valueint;
		fwrite(&componentLimitNumber, sizeof(uint32), 1, file);

		componentLimit = componentLimit->next;
	}

	cJSON *activeCamera = cJSON_GetObjectItem(json, "active_camera");
	writeStringAsUUID(activeCamera->valuestring, file);

	cJSON *gravity = cJSON_GetObjectItem(json, "gravity");
	real32 gravityValue = gravity->valuedouble;
	fwrite(&gravityValue, sizeof(real32), 1, file);

	fclose(file);
	cJSON_Delete(json);

	return 0;
}

void writeSystemGroup(const cJSON *systemGroup, FILE *file) {
	writeString(systemGroup->string, file);

	cJSON *externalSystems = cJSON_GetObjectItem(systemGroup, "external");

	uint32 numExternalSystems = getJSONListSize(externalSystems);
	fwrite(&numExternalSystems, sizeof(uint32), 1, file);

	cJSON *externalSystem = externalSystems->child;
	while (externalSystem) {
		writeString(externalSystem->valuestring, file);
		externalSystem = externalSystem->next;
	}

	cJSON *internalSystems = cJSON_GetObjectItem(systemGroup, "internal");

	uint32 numInternalSystems = getJSONListSize(internalSystems);
	fwrite(&numInternalSystems, sizeof(uint32), 1, file);

	cJSON *internalSystem = internalSystems->child;
	while (internalSystem) {
		writeString(internalSystem->valuestring, file);
		internalSystem = internalSystem->next;
	}
}