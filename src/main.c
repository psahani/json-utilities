#include "definitions.h"
#include "utilities.h"
#include "file_utilities.h"

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <time.h>
#include <stdlib.h>

internal void showHelp(char *utilityName, char *extension);
internal int32 showError(char *utilityName, char *extension);

int32 main(int32 argc, char *argv[]) {
	int32 error = 0;

	srand(time(0));

	char *extension = "";

	#if defined _WIN32
	extension = ".exe";
	#endif

	int32 (*utility)(const char*, Log) = NULL;
	char *utilityName = "";

	#if defined _ENTITY_GENERATOR
	utility = &generateEntity;
	utilityName = "entity_generator";
	char *generatedFilename = "new_entity";
	#elif defined _ENTITY_EXPORTER
	utility = &exportEntity;
	utilityName = "entity_exporter";
	#elif defined _ASSET_EXPORTER
	utility = &exportAsset;
	utilityName = "asset_exporter";
	#elif defined _SCENE_GENERATOR
	utility = &generateScene;
	utilityName = "scene_generator";
	char *generatedFilename = "new_scene";
	#elif defined _SCENE_EXPORTER
	utility = &exportScene;
	utilityName = "scene_exporter";
	#endif

	switch (argc - 1) {
		#if defined _ENTITY_GENERATOR || defined _SCENE_GENERATOR
		case 0:
			error = (*utility)(generatedFilename, (Log)&printf);
			break;
		#endif
		case 1:
			if (!strcmp(argv[1], "--help")) {
				showHelp(utilityName, extension);
			} else {
				char *extension = getExtension(argv[1]);
				if (extension && !strcmp(extension, "json")) {
					char *filename = removeExtension(argv[1]);
					error = (*utility)(filename, (Log)&printf);

					free(filename);
					free(extension);
				} else {
					error = (*utility)(argv[1], (Log)&printf);
				}
			}
			break;
		default:
			error = showError(utilityName, extension);
			break;
	}

	return error;
}

void showHelp(char *utilityName, char *extension) {
	printf("Usage:\n");

	#if defined _ENTITY_GENERATOR || defined _SCENE_GENERATOR
	printf("%s%s\n", utilityName, extension);
	#endif

	printf("%s%s [FILENAME]\n", utilityName, extension);
}

int32 showError(char *utilityName, char *extension) {
	printf("Invalid arguments.\n");
	printf("Try '%s%s --help' for more information.\n", utilityName, extension);

	return -1;
}
