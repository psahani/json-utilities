#include "utilities.h"
#include "file_utilities.h"
#include "entity_utilities.h"

#include <cjson/cJSON.h>

#include "malloc.h"

internal UUID generateSceneFile(const char *filename, Log log);
internal void generateCameraEntity(
	UUID uuid,
	const char *entitiesFolder,
	Log log
);

internal void generateDirectionalLightEntity(
	const char *entitiesFolder,
	Log log
);

internal void generateCubemapEntity(const char *entitiesFolder, Log log);

int32 generateScene(const char *filename, Log log) {
	deleteFolder(filename, false, log);
	MKDIR(filename);

	UUID camera = generateSceneFile(filename, log);

	char *entitiesFolder = getFullFilePath("entities", NULL, filename);
	MKDIR(entitiesFolder);

	generateCameraEntity(camera, entitiesFolder, log);
	generateDirectionalLightEntity(entitiesFolder, log);
	generateCubemapEntity(entitiesFolder, log);

	char *prototypesFolder = getFullFilePath(
		"prototypes",
		NULL,
		entitiesFolder
	);

	free(entitiesFolder);

	copyFolder("prototypes", prototypesFolder, log);
	free(prototypesFolder);

	return 0;
}

UUID generateSceneFile(const char *filename, Log log) {
	cJSON *json = cJSON_CreateObject();

	cJSON *systems = cJSON_AddObjectToObject(json, "systems");

	cJSON *updateSystems = cJSON_AddObjectToObject(systems, "update");

	const char *externalUpdateSystemsList = "basic_input";
	cJSON *externalUpdateSystems = cJSON_CreateStringArray(
		&externalUpdateSystemsList,
		1
	);

	cJSON_AddItemToObject(updateSystems, "external", externalUpdateSystems);

	const char *internalUpdateSystemsList[] = {
		"clean_hit_information",
		"clean_hit_list",
		"animation",
		"clean_global_transforms",
		"apply_parent_transforms",
		"simulate_rigid_bodies",
		"joint_information",
		"particle_simulator",
		"gui",
		"audio",
		"lights"
	};

	cJSON *internalUpdateSystems = cJSON_CreateStringArray(
		internalUpdateSystemsList,
		11
	);

	cJSON_AddItemToObject(updateSystems, "internal", internalUpdateSystems);

	cJSON *drawSystems = cJSON_AddObjectToObject(systems, "draw");
	cJSON_AddArrayToObject(drawSystems, "external");

	const char *internalDrawSystemsList[] = {
		"shadows",
		"cubemap_renderer",
		"render_heightmap",
		"renderer",
		"wireframe_renderer",
		"debug_renderer",
		"collision_primitive_renderer",
		"particle_renderer",
		"post_processing",
		"gui_renderer"
	};

	cJSON *internalDrawSystems = cJSON_CreateStringArray(
		internalDrawSystemsList,
		10
	);

	cJSON_AddItemToObject(drawSystems, "internal", internalDrawSystems);

	cJSON *componentLimits = cJSON_AddObjectToObject(json, "component_limits");
	cJSON_AddNumberToObject(componentLimits, "animation", 2048);
	cJSON_AddNumberToObject(componentLimits, "animator", 2048);
	cJSON_AddNumberToObject(componentLimits, "audio_source", 1024);
	cJSON_AddNumberToObject(componentLimits, "ball_socket_joint", 256);
	cJSON_AddNumberToObject(componentLimits, "ball_socket2_joint", 256);
	cJSON_AddNumberToObject(componentLimits, "box", 1024);
	cJSON_AddNumberToObject(componentLimits, "button", 1024);
	cJSON_AddNumberToObject(componentLimits, "camera", 8);
	cJSON_AddNumberToObject(componentLimits, "capsule", 1024);
	cJSON_AddNumberToObject(componentLimits, "collision_tree_node", 4096);
	cJSON_AddNumberToObject(componentLimits, "collision", 1024);
	cJSON_AddNumberToObject(componentLimits, "cubemap", 1);
	cJSON_AddNumberToObject(componentLimits, "debug_collision_primitive", 2048);
	cJSON_AddNumberToObject(componentLimits, "debug_line", 2048);
	cJSON_AddNumberToObject(componentLimits, "debug_point", 4096);
	cJSON_AddNumberToObject(componentLimits, "debug_primitive", 4096);
	cJSON_AddNumberToObject(componentLimits, "debug_transform", 1024);
	cJSON_AddNumberToObject(componentLimits, "font", 1024);
	cJSON_AddNumberToObject(componentLimits, "gui_transform", 2048);
	cJSON_AddNumberToObject(componentLimits, "heightmap", 256);
	cJSON_AddNumberToObject(componentLimits, "hinge_joint", 256);
	cJSON_AddNumberToObject(componentLimits, "hinge2_joint", 256);
	cJSON_AddNumberToObject(componentLimits, "hit_information", 4096);
	cJSON_AddNumberToObject(componentLimits, "hit_list", 8192);
	cJSON_AddNumberToObject(componentLimits, "image", 1024);
	cJSON_AddNumberToObject(componentLimits, "joint_constraint", 256);
	cJSON_AddNumberToObject(componentLimits, "joint_information", 256);
	cJSON_AddNumberToObject(componentLimits, "joint", 8192);
	cJSON_AddNumberToObject(componentLimits, "light", 128);
	cJSON_AddNumberToObject(componentLimits, "model", 2048);
	cJSON_AddNumberToObject(componentLimits, "next_animation", 2048);
	cJSON_AddNumberToObject(componentLimits, "panel", 512);
	cJSON_AddNumberToObject(componentLimits, "particle", 16384);
	cJSON_AddNumberToObject(componentLimits, "particle_emitter", 32);
	cJSON_AddNumberToObject(componentLimits, "progress_bar", 1024);
	cJSON_AddNumberToObject(componentLimits, "rigid_body", 1024);
	cJSON_AddNumberToObject(componentLimits, "slider_joint", 256);
	cJSON_AddNumberToObject(componentLimits, "slider", 1024);
	cJSON_AddNumberToObject(componentLimits, "sphere", 1024);
	cJSON_AddNumberToObject(componentLimits, "surface_information", 512);
	cJSON_AddNumberToObject(componentLimits, "text_field", 1024);
	cJSON_AddNumberToObject(componentLimits, "text", 1024);
	cJSON_AddNumberToObject(componentLimits, "transform", 16384);
	cJSON_AddNumberToObject(componentLimits, "widget", 1024);
	cJSON_AddNumberToObject(componentLimits, "wireframe", 2048);

	UUID camera = generateUUID();
	cJSON_AddStringToObject(json, "active_camera", camera.string);
	cJSON_AddNumberToObject(json, "gravity", -9.81);

	char *jsonFilename = getFullFilePath(filename, NULL, filename);
	writeJSON(json, jsonFilename, true, log);
	free(jsonFilename);

	cJSON_Delete(json);

	return camera;
}

void generateCameraEntity(UUID uuid, const char *entitiesFolder, Log log) {
	cJSON *json = cJSON_CreateObject();

	cJSON_AddStringToObject(json, "uuid", uuid.string);

	cJSON *components = cJSON_AddObjectToObject(json, "components");

	real32 position[] = { 0.0f, 0.0f, 10.0f };
	addTransform(components, position, NULL, NULL, NULL, NULL, NULL);

	cJSON *camera = cJSON_AddArrayToObject(components, "camera");

	addRealNumber(camera, "near plane", "float32", 0.01);
	addRealNumber(camera, "far plane", "float32", 1000.0);
	addRealNumber(camera, "aspect ratio", "float32", 1.0);
	addRealNumber(camera, "field of view", "float32", 80.0);

	real32 bounds[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	addFloatArray(camera, "bounds", 4, bounds);

	addInteger(camera, "projection type", "enum", 0);

	char *jsonFilename = getFullFilePath("camera", NULL, entitiesFolder);
	writeJSON(json, jsonFilename, true, log);
	free(jsonFilename);

	cJSON_Delete(json);
}

void generateDirectionalLightEntity(const char *entitiesFolder, Log log) {
	cJSON *json = cJSON_CreateObject();

	cJSON_AddStringToObject(json, "uuid", generateUUID().string);

	cJSON *components = cJSON_AddObjectToObject(json, "components");

	real32 rotation[] = { -0.383f, 0.0f, 0.0f, 0.924f };
	addTransform(components, NULL, rotation, NULL, NULL, NULL, NULL);

	cJSON *light = cJSON_AddArrayToObject(components, "light");

	addBool(light, "enabled", true);
	addInteger(light, "type", "enum", 0);

	real32 color[] = { 10.0f, 10.0f, 10.0f };
	addFloatArray(light, "radiant flux", 3, color);

	addRealNumber(light, "radius", "float32", 0.0);

	real32 size[] = { 1.0f, 1.0f };
	addFloatArray(light, "size", 2, size);

	char *jsonFilename = getFullFilePath(
		"directional_light",
		NULL,
		entitiesFolder
	);

	writeJSON(json, jsonFilename, true, log);
	free(jsonFilename);

	cJSON_Delete(json);
}

void generateCubemapEntity(const char *entitiesFolder, Log log) {
	cJSON *json = cJSON_CreateObject();

	cJSON_AddStringToObject(json, "uuid", generateUUID().string);

	cJSON *components = cJSON_AddObjectToObject(json, "components");

	cJSON *cubemap = cJSON_AddArrayToObject(components, "cubemap");

	addString(cubemap, "name", 64, "");

	char *jsonFilename = getFullFilePath("cubemap", NULL, entitiesFolder);
	writeJSON(json, jsonFilename, true, log);
	free(jsonFilename);

	cJSON_Delete(json);
}