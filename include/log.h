#pragma once

#include "definitions.h"

#include "file_utilities.h"

#define LOG(log, format, ...) if (log) { \
	(*log)(format, __VA_ARGS__); \
}