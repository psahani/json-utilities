PROJ = json-utilities
ENTITY_GENERATOR_NAME = entity-generator
ENTITY_EXPORTER_NAME = entity-exporter
ASSET_EXPORTER_NAME = asset-exporter
SCENE_GENERATOR_NAME = scene-generator
SCENE_EXPORTER_NAME = scene-exporter

IDIRS = include vendor

SRCDIR = src

BUILDDIR = build
OBJDIR = $(BUILDDIR)/obj
ENTITY_GENERATOR_OBJDIR = $(OBJDIR)/entity_generator
ENTITY_EXPORTER_OBJDIR = $(OBJDIR)/entity_exporter
ASSET_EXPORTER_OBJDIR = $(OBJDIR)/asset_exporter
SCENE_GENERATOR_OBJDIR = $(OBJDIR)/scene_generator
SCENE_EXPORTER_OBJDIR = $(OBJDIR)/scene_exporter

UTILITIES_RELEASE_DIR = release/utilities

_LIBDIRS = lib
LIBDIRS = $(foreach LIBDIR,$(_LIBDIRS),-L$(LIBDIR))

CC = clang
CFLAGS = $(foreach DIR,$(IDIRS),-I$(DIR)) -fPIC
DBFLAGS = -g -D_DEBUG -O0 -Wall
RELFLAGS = -O3
ENTITY_GENERATOR_FLAGS = -D_ENTITY_GENERATOR
ENTITY_EXPORTER_FLAGS = -D_ENTITY_EXPORTER
ASSET_EXPORTER_FLAGS = -D_ASSET_EXPORTER
SCENE_GENERATOR_FLAGS = -D_SCENE_GENERATOR
SCENE_EXPORTER_FLAGS = -D_SCENE_EXPORTER
SHAREDFLAGS = -shared

_LIBS = file-utilities cjson frozen
LIBS = $(foreach LIB,$(_LIBS),-l$(LIB))

DEPS = $(shell find include -name *.h)
VENDORDEPS = $(shell find vendor -name *.h)

OBJ = $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(shell find $(SRCDIR) -name *.c -not -name main.c))

$(OBJDIR)/%.o : $(SRCDIR)/%.c $(DEPS)
	$(CC) $(CFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) -c -o $@ $<

.PHONY: build

build : $(OBJ) $(if $(LIBRARY),,$(if $(ENTITY_GENERATOR),$(ENTITY_GENERATOR_OBJDIR)/main.o,)$(if $(ENTITY_EXPORTER),$(ENTITY_EXPORTER_OBJDIR)/main.o,)$(if $(ASSET_EXPORTER),$(ASSET_EXPORTER_OBJDIR)/main.o,)$(if $(SCENE_GENERATOR),$(SCENE_GENERATOR_OBJDIR)/main.o,)$(if $(SCENE_EXPORTER),$(SCENE_EXPORTER_OBJDIR)/main.o,))
	$(CC) $(CFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(LIBDIRS) $(if $(LIBRARY),$(SHAREDFLAGS),) -o $(BUILDDIR)/$(if $(LIBRARY),lib$(PROJ).so,$(if $(ENTITY_GENERATOR),$(ENTITY_GENERATOR_NAME),)$(if $(ENTITY_EXPORTER),$(ENTITY_EXPORTER_NAME),)$(if $(ASSET_EXPORTER),$(ASSET_EXPORTER_NAME),)$(if $(SCENE_GENERATOR),$(SCENE_GENERATOR_NAME),)$(if $(SCENE_EXPORTER),$(SCENE_EXPORTER_NAME),)) $^ $(LIBS)

$(ENTITY_GENERATOR_OBJDIR)/main.o : $(SRCDIR)/main.c $(DEPS)
	$(CC) $(CFLAGS) $(ENTITY_GENERATOR_FLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) -c -o $@ $<

$(ENTITY_EXPORTER_OBJDIR)/main.o : $(SRCDIR)/main.c $(DEPS)
	$(CC) $(CFLAGS) $(ENTITY_EXPORTER_FLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) -c -o $@ $<

$(ASSET_EXPORTER_OBJDIR)/main.o : $(SRCDIR)/main.c $(DEPS)
	$(CC) $(CFLAGS) $(ASSET_EXPORTER_FLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) -c -o $@ $<

$(SCENE_GENERATOR_OBJDIR)/main.o : $(SRCDIR)/main.c $(DEPS)
	$(CC) $(CFLAGS) $(SCENE_GENERATOR_FLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) -c -o $@ $<

$(SCENE_EXPORTER_OBJDIR)/main.o : $(SRCDIR)/main.c $(DEPS)
	$(CC) $(CFLAGS) $(SCENE_EXPORTER_FLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) -c -o $@ $<

.PHONY: buildall

buildall :
	@make ENTITY_GENERATOR=yes
	@make ENTITY_EXPORTER=yes
	@make ASSET_EXPORTER=yes
	@make SCENE_GENERATOR=yes
	@make SCENE_EXPORTER=yes

.PHONY: library

library : clean
	@make$(if $(WINDOWS), windows,) LIBRARY=yes RELEASE=yes

.PHONY: clean

clean:
	rm -rf release
	rm -rf $(BUILDDIR)
	mkdir -p $(ENTITY_GENERATOR_OBJDIR)
	mkdir -p $(ENTITY_EXPORTER_OBJDIR)
	mkdir -p $(ASSET_EXPORTER_OBJDIR)
	mkdir -p $(SCENE_GENERATOR_OBJDIR)
	mkdir -p $(SCENE_EXPORTER_OBJDIR)

.PHONY: rebuildall

rebuildall : clean buildall

.PHONY: release

release : clean
	@make $(if $(WINDOWS), windowsall,buildall) RELEASE=yes
	mkdir -p release/$(if $(WINDOWS),,bin $(UTILITIES_RELEASE_DIR))
	find build/* -type f -not -path '*/obj/*' -exec cp {} release/$(if $(WINDOWS),,bin/) \;
	cp README.md release/
	$(if $(WINDOWS),cp -r prototypes/ release/,cp -r prototypes/ $(UTILITIES_RELEASE_DIR))
	$(if $(WINDOWS),,find release/bin/* -exec mv {} {}-bin \;)
	$(if $(WINDOWS),,cp -r lib/ release/)
	$(if $(WINDOWS),,echo '#!/bin/bash' > $(UTILITIES_RELEASE_DIR)/$(ENTITY_GENERATOR_NAME) && echo 'LD_LIBRARY_PATH=../lib ../bin/$(ENTITY_GENERATOR_NAME)-bin $$@' >> $(UTILITIES_RELEASE_DIR)/$(ENTITY_GENERATOR_NAME) && chmod +x $(UTILITIES_RELEASE_DIR)/$(ENTITY_GENERATOR_NAME))
	$(if $(WINDOWS),,echo '#!/bin/bash' > $(UTILITIES_RELEASE_DIR)/$(ENTITY_EXPORTER_NAME) && echo 'LD_LIBRARY_PATH=../lib ../bin/$(ENTITY_EXPORTER_NAME)-bin $$@' >> $(UTILITIES_RELEASE_DIR)/$(ENTITY_EXPORTER_NAME) && chmod +x $(UTILITIES_RELEASE_DIR)/$(ENTITY_EXPORTER_NAME))
	$(if $(WINDOWS),,echo '#!/bin/bash' > $(UTILITIES_RELEASE_DIR)/$(ASSET_EXPORTER_NAME) && echo 'LD_LIBRARY_PATH=../lib ../bin/$(ASSET_EXPORTER_NAME)-bin $$@' >> $(UTILITIES_RELEASE_DIR)/$(ASSET_EXPORTER_NAME) && chmod +x $(UTILITIES_RELEASE_DIR)/$(ASSET_EXPORTER_NAME))
	$(if $(WINDOWS),,echo '#!/bin/bash' > $(UTILITIES_RELEASE_DIR)/$(SCENE_GENERATOR_NAME) && echo 'LD_LIBRARY_PATH=../lib ../bin/$(SCENE_GENERATOR_NAME)-bin $$@' >> $(UTILITIES_RELEASE_DIR)/$(SCENE_GENERATOR_NAME) && chmod +x $(UTILITIES_RELEASE_DIR)/$(SCENE_GENERATOR_NAME))
	$(if $(WINDOWS),,echo '#!/bin/bash' > $(UTILITIES_RELEASE_DIR)/$(SCENE_EXPORTER_NAME) && echo 'LD_LIBRARY_PATH=../lib ../bin/$(SCENE_EXPORTER_NAME)-bin $$@' >> $(UTILITIES_RELEASE_DIR)/$(SCENE_EXPORTER_NAME) && chmod +x $(UTILITIES_RELEASE_DIR)/$(SCENE_EXPORTER_NAME))

WINCC = x86_64-w64-mingw32-clang
WINCFLAGS = $(foreach DIR,$(IDIRS),-I$(DIR))
WINFLAGS = -I/usr/local/include -Wl,-subsystem,windows
_WINLIBS = file-utilities cjson frozen
WINLIBS = $(foreach LIB,$(_WINLIBS),-l$(LIB))

_WINLIBDIRS = winlib
WINLIBDIRS = $(foreach LIBDIR,$(_WINLIBDIRS),-L$(LIBDIR))

WINOBJ = $(patsubst %.o,%.obj,$(OBJ))

$(OBJDIR)/%.obj : $(SRCDIR)/%.c $(DEPS)
	$(WINCC) $(WINCFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(WINFLAGS) -c -o $@ $<

.PHONY: windows

windows : $(WINOBJ) $(if $(LIBRARY),,$(if $(ENTITY_GENERATOR),$(ENTITY_GENERATOR_OBJDIR)/main.obj,)$(if $(ENTITY_EXPORTER),$(ENTITY_EXPORTER_OBJDIR)/main.obj,)$(if $(ASSET_EXPORTER),$(ASSET_EXPORTER_OBJDIR)/main.obj,)$(if $(SCENE_GENERATOR),$(SCENE_GENERATOR_OBJDIR)/main.obj,)$(if $(SCENE_EXPORTER),$(SCENE_EXPORTER_OBJDIR)/main.obj,))
	$(WINCC) $(WINCFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(WINFLAGS) $(WINLIBDIRS) $(if $(LIBRARY),$(SHAREDFLAGS),) -o $(BUILDDIR)/$(if $(LIBRARY),$(PROJ).dll,$(if $(ENTITY_GENERATOR),$(ENTITY_GENERATOR_NAME).exe,)$(if $(ENTITY_EXPORTER),$(ENTITY_EXPORTER_NAME).exe,)$(if $(ASSET_EXPORTER),$(ASSET_EXPORTER_NAME).exe,)$(if $(SCENE_GENERATOR),$(SCENE_GENERATOR_NAME).exe,)$(if $(SCENE_EXPORTER),$(SCENE_EXPORTER_NAME).exe,)) $^ $(WINLIBS)
	cp winlib/* $(BUILDDIR)/

$(ENTITY_GENERATOR_OBJDIR)/main.obj : $(SRCDIR)/main.c $(DEPS)
	$(WINCC) $(WINCFLAGS) $(ENTITY_GENERATOR_FLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(WINFLAGS) -c -o $@ $<

$(ENTITY_EXPORTER_OBJDIR)/main.obj : $(SRCDIR)/main.c $(DEPS)
	$(WINCC) $(WINCFLAGS) $(ENTITY_EXPORTER_FLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(WINFLAGS) -c -o $@ $<

$(ASSET_EXPORTER_OBJDIR)/main.obj : $(SRCDIR)/main.c $(DEPS)
	$(WINCC) $(WINCFLAGS) $(ASSET_EXPORTER_FLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(WINFLAGS) -c -o $@ $<

$(SCENE_GENERATOR_OBJDIR)/main.obj : $(SRCDIR)/main.c $(DEPS)
	$(WINCC) $(WINCFLAGS) $(SCENE_GENERATOR_FLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(WINFLAGS) -c -o $@ $<

$(SCENE_EXPORTER_OBJDIR)/main.obj : $(SRCDIR)/main.c $(DEPS)
	$(WINCC) $(WINCFLAGS) $(SCENE_EXPORTER_FLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(WINFLAGS) -c -o $@ $<

.PHONY: windowsall

windowsall :
	@make windows ENTITY_GENERATOR=yes
	@make windows ENTITY_EXPORTER=yes
	@make windows ASSET_EXPORTER=yes
	@make windows SCENE_GENERATOR=yes
	@make windows SCENE_EXPORTER=yes
